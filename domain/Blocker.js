'use strict';


class Blocker {

  constructor(user, desc) {
      this.user = user;
      this.desc = desc;
  }

  user() {
    return this.user;
  }

  desc() {
    return this.desc;
  }

}


module.exports = Blocker;
