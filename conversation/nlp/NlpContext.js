

class NlpContext{



  constructor(user, text) {
    var _this = this;
    _this.user = user;
    _this.text = text;
  }



  getUserHandle() {
    return _this.user.handle;
  }

  respond() {
    return 'ok, ' + _this.user.name;
  }
}

exports = NlpContext;
