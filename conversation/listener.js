
var CommandContext = require('./commands/CommandContext.js');
var NlpConext = require('./nlp/NlpContext.js');
var trackerDB = require('../datastore/tracker.js');


exports.interpret = function(user, text) {
  if (text.indexOf('/') == 0) {
    return new CommandContext(trackerDB, user, text);
  } else {
    return new NlpContext(trackerDB);
  }
}
