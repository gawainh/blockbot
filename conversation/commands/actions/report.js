'use strict';


class ReportAction {

  constructor(tracker, user, desc) {
      this.tracker = tracker;
      this.user = user;
      this.desc = desc;
  }

  execute() {
    var report = this.tracker.report();
    this.report = report;
    return report;
  }

  response() {
    var msg = 'ok, ' + this.user.name + '. These are the current blockers: \n';
    for (var i = 0 ; i < this.report.length ; i++) {
      var blocker  = this.report[i];
      msg += '  ' + i + ': ' + blocker.desc + ' ' + blocker.status() + '\n';
    }
    return msg;
  }

}


module.exports = ReportAction;
