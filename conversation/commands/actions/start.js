'use strict';


class StartBlockerAction {

  constructor(tracker, user, desc) {
      this.tracker = tracker;
      this.user = user;
      this.desc = desc;
  }

  execute() {
    var id = this.tracker.startBlocker(this.desc, this.user);
    this.blockerId = id;
    return id;
  }

  response() {
    var msg = 'ok, ' + this.user.name + '. I have created a new blocker for you with description: "' + this.desc + '", and id ' + this.blockerId;
    return msg;
  }

}


module.exports = StartBlockerAction;
