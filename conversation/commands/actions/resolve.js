'use strict';


class ResolveBlockerAction {

  constructor(tracker, user, text) {
      this.tracker = tracker;
      this.user = user;
      this.text = text;
  }

  execute() {
    var blocker = this.tracker.resolveBlocker(this.text);
    this.blocker = blocker;
    return blocker;
  }

  response() {
    var msg = 'ok, ' + this.user.name + '. I have resolved blocker with description: "' + this.blocker.desc + '"';
    return msg;
  }

}


module.exports = ResolveBlockerAction;
