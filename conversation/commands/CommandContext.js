'use strict';

var _ = require('underscore');



/**
 * Command represents a direct command from a slack user, typically starting with a '/'
 *
 * A bit like a shell comannd:
 * 1 disambiguate command
 * 2 execute command
 * 3 return command response
 *
 * EG: given text "/start corprate wifi down" the CommandContext Will execute
 * the 'start' command which creates a new blocker with
 * description "coporate wifi down" and return an appropriate chat style response
 *
 */
class CommandContext{


  constructor(trackerDB, user, text) {
    var _this = this;
    this.trackerDB = trackerDB;
    this.user = user;
    this.fullText = text;
    var processedText = processText(text);
    this.desc = processedText.args;
    this.command = processedText.cmd;
    //console.log(JSON.stringify(this));
  }



  getUserHandle() {
    return this.user.handle;
  }

  actAndRespond() {
    var Action = require('./actions/' + this.command);
    var action = new Action(this.trackerDB, this.user, this.desc);
    var responseData = action.execute();
    var textResponse = action.response();
    return {responseData: responseData, textResponse: textResponse};
  }

}

module.exports = CommandContext;


function processText(text) {

  var wordArray = text.split(' ');
  var command = wordArray.shift().split('/')[1];

  var cmdArguments = _.reduce(wordArray, function(memo, word, i){
    if (i < wordArray.length -1) {
      return memo += word + ' ';
    } else {
      return memo += word;
    }

  }, '');

  return {cmd: command, args: cmdArguments};

}
