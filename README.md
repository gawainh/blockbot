# blockbot
slack blocker reporting bot



# pre-requisites

install nodejs https://nodejs.org/en/download/



# getting started

```
make configure
```


# run blockerbot

```
make run
```


# run tests like a pro

```
make tests
```
