var uuid = require('node-uuid');
var dateMath = require('date-arithmetic');
var Elapsed = require('elapsed');
var clone = require('clone-deep');




var blockers = {};

exports.startBlocker = function(desc, user) {
  var id = uuid.v4();
  var startDate = new Date();
  var blocker = {
    desc: desc,
    user: user,
    ended: undefined,
    started: startDate,
    elapsed: function() {
      var elapsed = new Elapsed(startDate, new Date());
      var time = elapsed.hours.num;
      return (time + 1) + 'h';
    },
    status: function() {
      if (blocker.ended == undefined) {
        return "[open]";
      } else {
        return "[closed]";
      }
    }
  }
  blockers[id] = blocker;
  return id;
}

exports.getBlocker = function(id) {
  var blocker = blockers[id];
  return clone(blocker);
}


exports.report = function(id) {
  var blockerArray = [];
  for (blockerId in blockers) {
    var blocker = blockers[blockerId];
    blockerArray.push(blocker);
  }
  return blockerArray;
}


exports.resolveBlocker = function(id) {
  var blocker = blockers[id];
  if (! blocker || blocker.ended) {
    return undefined;
  } else {
      blocker.ended = new Date();
      return blocker;
  }
}

exports.reset = function() {
  blockers = {};
}
