var assert = require('assert');
var expect = require('expect.js');
var Elapsed = require('elapsed');
var tracker = require('../../datastore/tracker.js');

describe("blocker tracker test:", function () {

    beforeEach(function () {
      tracker.reset();
    });

    after(function () {
        //
    });

    afterEach(function () {
        //
    });

    it("can start and resolve a new blocker", function () {

          // start a new blocker
          var blockerId = tracker.startBlocker("pub open", "gawain");
          var blocker = tracker.getBlocker(blockerId);
          assert.equal(blocker.user, "gawain", "blocker user expected to be 'gawain' but was " + blocker.user);
          assert.equal(blocker.desc, "pub open", "blocker user expected to be 'pub open' but was " + blocker.desc);
          assert.equal(blocker.elapsed(), "1h", "blocker user expected to be '1h' but was " + blocker.elapsed());

          // resolve blocker
          var resolvedBlocker = tracker.resolveBlocker(blockerId);
          assert(resolvedBlocker.ended, "blocker expected to be resolved and have ended date set but returned:  " + resolvedBlocker.ended);

          // attempt to re-resolve blocker (an illegal action)
          var unresolvedBlocker = tracker.resolveBlocker(blockerId);
          assert(! unresolvedBlocker, "blocker expected not to be re-resolveable (should therefore return undefined) but returned:  " + JSON.stringify(unresolvedBlocker));

          // re-retreive blocker and check resolve date has not increased due to attempted re-resolve
          var reResolvedBlocker = tracker.getBlocker(blockerId);
          var elapsed = new Elapsed(resolvedBlocker.ended, reResolvedBlocker.ended);
          var time = elapsed.seconds.num;
          assert.equal(0, time, "re-resolving blocker should maintain previously resolved date, but " + time + " seconds have elapsed");

    });

    it("reports on blocker status", function () {


          // start a new blocker
          var blocker1Id = tracker.startBlocker("sandbox is down", "david");
          var blocker2Id = tracker.startBlocker("on prem is down", "jim");
          var blocker3Id = tracker.startBlocker("amazon is down", "bob");

          // assertr report data
          var report = tracker.report();
          //console.dir(report);
          assert.equal(report.length, 3, "expected tracker report to return 3 blockers");


    });

});
