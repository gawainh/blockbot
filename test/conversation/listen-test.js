var listener = require('../../conversation/listener.js');
var assert = require('assert');
var expect = require('expect.js');
var Elapsed = require('elapsed');


describe("conversational listener test:", function () {


    before(function () {

    });

    after(function () {
        //
    });

    afterEach(function () {
        //
    });

    it("can interpret /start command and return correct context", function () {
        var user = {
          name: 'robert',
          handle: '@bob',
          id: '123'
        };
        var context = listener.interpret(user, "/start bay doors are locked");
        var response = context.actAndRespond();

        expectedMsg = 'ok, robert. I have created a new blocker for you with description: "bay doors are locked", and id ' + response.responseData;

        assert.equal(response.textResponse, expectedMsg, "context response not as expected, should be '" + expectedMsg + "'");

    });

    it("can interpret /resolve command and return correct response", function () {
        var user = {
          name: 'robert',
          handle: '@bob',
          id: '123'
        };

        var context = listener.interpret(user, "/start Computer says no");
        var response = context.actAndRespond();
        var newBlockerId = response.responseData;

        var context = listener.interpret(user, "/resolve " + newBlockerId);
        var response = context.actAndRespond();

        expectedMsg = 'ok, robert. I have resolved blocker with description: "Computer says no"';

        assert.equal(response.textResponse, expectedMsg, "context response not as expected, should be '" + expectedMsg + "'");

    });

    it("can interpret /report and return reporting response", function () {
        var user = {
          name: 'robert',
          handle: '@bob',
          id: '123'
        };

        var context = listener.interpret(user, "/report");
        var response = context.actAndRespond();
        var responseData = response.responseData;
        var responseMsg = response.textResponse;


        //console.log(response);

        assert(responseMsg.indexOf('bay doors are locked [open]') > -1, "report response did not comtain 'bay doors are locked'");
        assert(responseMsg.indexOf('Computer says no [closed]') > -1, "report response did not comtain 'bay doors are locked'");

    });

});


function isUuid(string) {
    return (string.length == 36);
}
